#!/usr/bin/env ruby

# Leave this one empty to place the templates in the library folder
directory  = "-d ./generated-templates/"

# For options, see makefile.erb
programmer = "-p usbasp"
chips = [
  '-n ATtiny13 -c attiny13 -a t13 -k 1200000 -f "-U lfuse:w:0x6a:m -U hfuse:w:0xff:m"',
  '-n ATtiny13A -c attiny13a -a t13 -k 1200000 -f "-U lfuse:w:0x6a:m -U hfuse:w:0xff:m"',
  '-n ATtiny2313 -c attiny1313 -a t2313 -k 1000000 -f "-U lfuse:w:0x64:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m"',
  '-n ATmega88 -c atmega88 -a m88 -k 1000000 -f "-U lfuse:w:0x62:m -U hfuse:w:0xdf:m -U efuse:w:0xf9:m"',
  '-n ATmega328 -c atmega328 -a m328 -k 1000000 -f "-U lfuse:w:0x62:m -U hfuse:w:0xd9:m -U efuse:w:0xff:m"',
]
chips.each do |chip|
  cmd = "./generate.rb #{chip} #{directory} #{programmer}"
  puts "Running: #{cmd}"
  puts '> ' + `#{cmd}`
end