#!/usr/bin/env ruby
# This program creates an Xcode4 template for an Atmel AVR microcontroller in ~/Library/Developer/Xcode/Templates

require 'optparse'
require 'digest'
require 'erb'
require 'fileutils'

# Default values
options = {
  :chp => '',
  :mcu => '',
  :pmc => '',
  :fus => '',
  :clk => '',
  :pro => '',
  :uid => '',
  :dir => ENV['HOME'] + '/Library/Developer/Xcode/Templates/Atmel AVR®/'
}

# Parse options
OptionParser.new do |opts|
  opts.banner =
    "This program creates an Xcode4 template for an Atmel AVR microcontroller\n\n"+
    "Usage: generator.rb [options]"
  
  opts.on('-n','--name NAME','Template name') { |o| options[:chp] = o }
  opts.on('-c','--chip CHIP','Chip model') { |o| options[:mcu] = o }
  opts.on('-a','--avrdude-chip AVRDUDECHIP','AVRDUDE chip model') { |o| options[:pmc] = o }
  opts.on('-f','--fuses FUSES','Fuses string') { |o| options[:fus] = o }
  opts.on('-k','--clock N','Clock frequency') { |o| options[:clk] = o }
  opts.on('-p','--programmer PROGRAMMER','Programmer') { |o| options[:pro] = o }
  opts.on('-d','--directory DIR','Template direcotry') { |o| options[:dir] = o }
  
  opts.on_tail('-h','--help', 'Show this help') do
    puts opts
    exit
  end
end.parse!

# Check missing options
raise OptionParser::MissingArgument if options[:chp] == ''
raise OptionParser::MissingArgument if options[:mcu] == ''

# Complete information
options[:uid] = Digest::MD5.hexdigest options[:chp]

# Paths to loop through
path = options[:dir] + '/' + options[:chp] + '.xctemplate/'
files = [
  'TemplateInfo.plist',
  'makefile',
  'main.c',
]

# Create directory
FileUtils.mkdir_p path

# Create files
files.each do |file|
  File.write(
    path + file,
    ERB.new(File.read(File.expand_path('../' + file + '.erb', __FILE__)), 0, '<>').result
  )
end

puts "Template for #{options[:chp]} generated"

