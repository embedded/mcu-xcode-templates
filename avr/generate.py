#! /usr/bin/env python
# coding=utf8
# This program creates an Xcode4 template for an Atmel AVR microcontroller in ~/Library/Developer/Xcode/Templates

import os, time, sys, getopt, hashlib

# Initialise as empty strings
chp = ''
mcu = ''
pmc = ''
fus = ''
clk = ''
pro = ''

# Check arguments
#try:
argv = sys.argv[1:]
opts, args = getopt.getopt(argv,'hn:c:a:f:k:p:',['help', 'chip', 'mcu', 'pmcu', 'avrdude-mcu', 'fuses', 'clock', 'programmer'])
#except getopt.GetoptError:
    # Nothing yet...
for opt, arg in opts:
    if opt in('-h', '--help'):
        print 'This program creates an Xcode4 template for an Atmel AVR microcontroller\n\navr-template-creator [-n|--name] [-c|--chip] [-a|--achip|--avrdude-chip] [-f|--fuses] [-k|--clock] [-p|--programmer]'
        sys.exit()
    elif opt in ('-n', '--name'):
        chp = arg
    elif opt in ('-c', '--chip'):
        mcu = arg
    elif opt in ('-a', '--achip', '--avrdude-chip'):
        pmc = arg
    elif opt in ('-f', '--fuses'):
        fus = arg
    elif opt in ('-k', '--clock'):
        clk = arg
    elif opt in ('-p', '--programmer'):
        pro = arg


# Enter chip information
if not chp: chp = raw_input('Chip/Template name: ')
if not mcu: mcu = raw_input('AVR-GCC MCU name: ')
if not pmc: pmc = raw_input('AVRdude MCU name: ')
if not fus: fus = raw_input('Default fuses (avrdude arguments): ')
if not clk: clk = raw_input('Default clock speed (without UL): ')
if not pro: pro = raw_input('Default programmer (usbasp, stk500v2 or arduino): ')

# Programmer
if pro == 'stk500v2':
    pro = 'PROGRAMMER_ID = stk500v2\nPROGRAMMER_PORT = avrdoper'
elif pro == 'usbasp':
    pro = 'PROGRAMMER_ID = usbasp\nPROGRAMMER_PORT = usb'
elif pro == 'arduino':
    pro = 'PROGRAMMER_ID = avrisp\nPROGRAMMER_PORT = com1'
else
    pro = 'PROGRAMMER_ID = \nPROGRAMMER_PORT = '

# Create neccesary folders
home = os.getenv("HOME")
path = home+'/Library/Developer/Xcode/Templates'
if not os.path.exists(path): os.makedirs(path)
path += '/Atmel AVR®'
if not os.path.exists(path): os.makedirs(path)
path += '/'+chp+'.xctemplate'
if not os.path.exists(path): os.makedirs(path)

# Assemble TemplateInfo.plist
plist = '''<?xml version="1.0" encoding="UTF-8"?>\n
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Kind</key>
	<string>Xcode.Xcode3.ProjectTemplateUnitKind</string>
	<key>Identifier</key>
	<string>xcode4.'''+hashlib.md5(chp).hexdigest()+'.'+mcu+'''</string>
	<key>Ancestors</key>
	<array>
		<string>com.apple.dt.unit.base</string>
	</array>
	<key>Concrete</key>
	<true/>
	<key>Description</key>
	<string>Atmel AVR® project for '''+chp+''' using avr-gcc.</string>
	<key>Definitions</key>
	<dict>
		<key>main.c</key>
		<dict>
			<key>Path</key>
			<string>main.c</string>
		</dict>
		<key>Makefile</key>
		<dict>
			<key>Path</key>
			<string>Makefile</string>
		</dict>
	</dict>
	<key>Nodes</key>
	<array>
		<string>main.c</string>
		<string>Makefile</string>
	</array>
	<key>Project</key>
	<dict>
		<key>SharedSettings</key>
		<dict>
			<key>PATH</key>
			<string>$(PATH):/usr/local/CrossPack-AVR/bin</string>
			<key>OTHER_CFLAGS</key>
			<string></string>
			<key>OTHER_LDFLAGS</key>
			<string></string>
		</dict>
	</dict>
	<key>Targets</key>
	<array>
		<dict>
			<key>Name</key>
			<string>build</string>
			<key>TargetType</key>
			<string>Legacy</string>
			<key>BuildToolPath</key>
			<string>/usr/local/CrossPack-AVR/bin/make</string>
			<key>BuildToolArgsString</key>
			<string>size clean</string>
			<key>BuildWorkingDirectory</key>
			<string>$(SRCROOT)/$(TARGETNAME)</string>
		</dict>
		<dict>
			<key>Name</key>
			<string>clean</string>
			<key>TargetType</key>
			<string>Legacy</string>
			<key>BuildToolPath</key>
			<string>/usr/local/CrossPack-AVR/bin/make</string>
			<key>BuildToolArgsString</key>
			<string>clean</string>
		</dict>
		<dict>
			<key>Name</key>
			<string>fuse</string>
			<key>TargetType</key>
			<string>Legacy</string>
			<key>BuildToolPath</key>
			<string>/usr/local/CrossPack-AVR/bin/make</string>
			<key>BuildToolArgsString</key>
			<string>writefuse</string>
		</dict>
		<dict>
			<key>Name</key>
			<string>flash</string>
			<key>TargetType</key>
			<string>Legacy</string>
			<key>BuildToolPath</key>
			<string>/usr/local/CrossPack-AVR/bin/make</string>
			<key>BuildToolArgsString</key>
			<string>writeflash clean</string>
		</dict>
	</array>
</dict>
</plist>
'''

# Assemble the makefile

makefile = '''#################################
#####		  ABOUT			#####
#### Simple makefile for AVR ####
###	 Based on online samples  ###
####						 ####
#####						#####
#################################

#####       SETTINGS        #####
####    Microcontroller      ####

MCU = '''+mcu+'''
CPU = '''+clk+'''UL
FUS = '''+fus+'''

####		Programmer		 ####

PROGRAMMER_MCU = '''+pmc+'''

PROGRAMMER_ID = stk500v2
PROGRAMMER_PORT = avrdoper

#PROGRAMMER_ID = usbasp
#PROGRAMMER_PORT = usb

####		Compiling		 ####

PROJECT_NAME = ___PROJECTNAME___
PROJECT_SOURCE = main.c
OPTLEVEL = s
INC =
LIBS =

#################################
#####  SETTINGS EXPLAINED   #####
####	Microcontroller		 ####
#
# MCU, Microcontroller Unit, the type of microcontroller you use, eg: atmega88
# CPU, Clockspeed in Hz followed by 'UL'
# FUS, fuse parameters for avrdude (http://www.engbedded.com/fusecalc/)
# 
####		Programmer		 ####
#
# PROGRAMMER_MCU, the name of your microcontroller for avrdude, eg: m88, m328, ...
# PROGRAMMER_ID, the id of your programmer, eg: stk500v2, usbasp, avrisp, ...
# PROGRAMMER_PORT, the port of your programmer, eg: avrdoper, usb, com1, ...
#
####		Compiling		 ####
#
# PROJECT_NAME, name of your project, some of the output files will be named after it
# PROJECT_SOURCE, main source file, eg: main.c
# OPTLEVEL, s (size), 1, 2, 3, or 0 (no optimalisation)
# INC, additional includes, eg: -lmylib
# LIBS, additional libraries, eg: -I/path/to/mydir
#
#################################


#####		COMPILER		#####
####		Flags			 ####

# Specify hex format. Eg: \'ihex\' for Intel Hex
HEXFORMAT=ihex

# Compiler Flags
CFLAGS=-I. $(INC) -g -mmcu=$(MCU) -DF_CPU=$(CPU) -O$(OPTLEVEL) -std=gnu99 -fpack-struct -fshort-enums -funsigned-bitfields -funsigned-char -Wall -gdwarf-2 -MD -MP -MT -Wa,-ahlms=$(firstword $(filter %.lst, $(<:.c=.lst)))

# C++ Specific Flags
CPPFLAGS=-fno-exceptions -Wa,-ahlms=$(firstword $(filter %.lst, $(<:.cpp=.lst))	$(filter %.lst, $(<:.cc=.lst)) $(filter %.lst, $(<:.C=.lst)))

# Assembler Flags
ASMFLAGS=-I. $(INC) -mmcu=$(MCU) -x assembler-with-cpp -Wa,-gstabs,-ahlms=$(firstword $(<:.S=.lst) $(<.s=.lst))

# LD Flags
LDFLAGS=-Wl,-Map,$(TRG).map -mmcu=$(MCU) -lm $(LIBS)


####		Commands		 ####

CC=avr-gcc
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump
SIZE=avr-size
AVRDUDE=avrdude
REMOVE=rm -f


####   Generated file names	 ####

TRG=$(PROJECT_NAME).out
DUMPTRG=$(PROJECT_NAME).s
HEXROMTRG=$(PROJECT_NAME).hex 
HEXTRG=$(HEXROMTRG) $(PROJECT_NAME).ee.hex
GDBINITFILE=gdbinit-$(PROJECT_NAME)


####   Define object files   ####

# Filter for C++
CPPFILES=$(filter %.cpp, $(PROJECT_SOURCE))
CCFILES=$(filter %.cc, $(PROJECT_SOURCE))
BIGCFILES=$(filter %.C, $(PROJECT_SOURCE))

# Filter for C
CFILES=$(filter %.c, $(PROJECT_SOURCE))

# Filter for Assembly
ASMFILES=$(filter %.S, $(PROJECT_SOURCE))

# List all object files to be created
OBJDEPS=$(CFILES:.c=.o) $(CPPFILES:.cpp=.o) $(BIGCFILES:.C=.o) $(CCFILES:.cc=.o) $(ASMFILES:.S=.o)

# List all \'list\' files
LST=$(filter %.lst, $(OBJDEPS:.o=.lst))

# List all Assembly files
GENASMFILES=$(filter %.s, $(OBJDEPS:.o=.s)) 

# Possible suffixes
.SUFFIXES : .c .cc .cpp .C .o .out .s .S .hex .ee.hex .h .hh .hpp

# 
.PHONY: writeflash clean stats gdbinit stats

####	  Make Targets		 ####
###			Legend			  ###
### all:		builds project
### disasm:		similar to stats
### stats:		shows projects stats
###	size:		builds and shows size
### hex:		creates hex files
###	writeflash:	builds and writes to device
###	install:	idem
###	writefuse:	writes specified fuses
### clean:		remove redundant files

all: $(TRG)

disasm: $(DUMPTRG) stats

stats: $(TRG)
	$(OBJDUMP) -h $(TRG)
	$(SIZE) $(TRG)

size: $(TRG)
	$(SIZE) -C --mcu=$(MCU) $(TRG)

hex: $(HEXTRG)


writeflash: hex
	$(AVRDUDE) -c $(PROGRAMMER_ID) -p $(PROGRAMMER_MCU) -P $(PROGRAMMER_PORT) -e -U flash:w:$(HEXROMTRG)

install: writeflash

writefuse:
	$(AVRDUDE) -c $(PROGRAMMER_ID) -p $(PROGRAMMER_MCU) -P $(PROGRAMMER_PORT) $(FUS)

$(DUMPTRG): $(TRG) 
	$(OBJDUMP) -S  $< > $@

$(TRG): $(OBJDEPS) 
	$(CC) $(LDFLAGS) -o $(TRG) $(OBJDEPS)


####	Generating assembly	 ####
# From C
%.s: %.c
	$(CC) -S $(CFLAGS) $< -o $@

# From manual ASM
%.s: %.S
	$(CC) -S $(ASMFLAGS) $< > $@

# From C++
.cpp.s .cc.s .C.s :
	$(CC) -S $(CFLAGS) $(CPPFLAGS) $< -o $@

#### Generating Object Files ####
# From C
.c.o: 
	$(CC) $(CFLAGS) -c $< -o $@

# From C++
.cc.o .cpp.o .C.o :
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

# From ASM
.S.o :
	$(CC) $(ASMFLAGS) -c $< -o $@

####   Generating hex files  ####
# From elf
.out.hex:
	$(OBJCOPY) -j .text -j .data -O $(HEXFORMAT) $< $@

.out.ee.hex:
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O $(HEXFORMAT) $< $@


#### Generating gdb init file ####
# Use by launching simulavr and avr-gdb: avr-gdb -x gdbinit-myprojec
gdbinit: $(GDBINITFILE)

$(GDBINITFILE): $(TRG)
	@echo "file $(TRG)" > $(GDBINITFILE)
	
	@echo "target remote localhost:1212" >> $(GDBINITFILE)
	
	@echo "load"        >> $(GDBINITFILE) 
	@echo "break main"  >> $(GDBINITFILE)
	@echo "continue"    >> $(GDBINITFILE)
	@echo
	@echo "Use \'avr-gdb -x $(GDBINITFILE)\'"


####		  Cleanup		 ####
clean:
	$(REMOVE) $(TRG) $(TRG).map $(DUMPTRG)
	$(REMOVE) $(OBJDEPS)
	$(REMOVE) $(LST) $(GDBINITFILE)
	$(REMOVE) $(GENASMFILES)
	$(REMOVE) $(HEXTRG)
	


#####			EOF			#####
'''

# Default program

main = '''/*
 * Author: ___FULLUSERNAME___
 * Date: ___DATE___
 * Chip: '''+chp+'''
 * Description:
 */

#include <avr/io.h>

int main(void) {
	
	while(1) {
		
	}
    return 1;
}
'''

# Write all files to disk
file = open(path+'/TemplateInfo.plist', 'w')
file.write(plist)
file.close()

file = open(path+'/makefile', 'w')
file.write(makefile)
file.close()

file = open(path+'/main.c', 'w')
file.write(main)
file.close()